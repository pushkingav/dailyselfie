package com.apushkin.dailyselfie;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SelfieItemsListAdapter extends BaseAdapter{

    private final List<SelfieItem> mItems = new ArrayList<SelfieItem>();
    private final Context mContext;

    public SelfieItemsListAdapter(Context context) {
        mContext = context;
    }

    public void add(SelfieItem item){

        mItems.add(item);
        notifyDataSetChanged();

    }

    public void clear(){

        mItems.clear();
        notifyDataSetChanged();

    }

    @Override
    public int getCount() {

        return mItems.size();

    }

    @Override
    public Object getItem(int position) {

        return mItems.get(position);

    }

    @Override
    public long getItemId(int position) {

        return position;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //Get current selfie item
        final SelfieItem selfieItem = (SelfieItem) getItem(position);

        LinearLayout itemLayout = (LinearLayout) convertView;

        if (itemLayout == null) {
            itemLayout = (LinearLayout) LayoutInflater.from(mContext)
                    .inflate(R.layout.row_layout, null);

        //Create a ViewHolder for smooth scrolling
            ViewHolder newSelfieHolder = new ViewHolder();
            newSelfieHolder.anImageView = (ImageView) itemLayout.findViewById(R.id.selphieThumbView);
            newSelfieHolder.aTextView = (TextView) itemLayout.findViewById(R.id.selphieNameView);

            itemLayout.setTag(newSelfieHolder);
        }

        ViewHolder selfieHolder = (ViewHolder) itemLayout.getTag();

        //Hardcoded values are here cause if using a ViewHolder we can't read the
        //imageView.getWidth() or getHeight() - it will be fixed in future releases :)

        SimpleDateFormat formatter = new SimpleDateFormat("EEE, MMM d, ''yy HH.mm.ss", Locale.US);      //MMMM.dd.yyyy HH:MM:SS
        selfieHolder.aTextView.setText(formatter.format(selfieItem.getDate()));

        selfieHolder.anImageView.setImageBitmap(setPic(50,
                50,selfieItem.getFilename()));
        return itemLayout;
    }

    public static Bitmap setPic(int viewWidth, int viewHeight, String mCurrentPhotoPath) {

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/viewWidth, photoH/viewHeight);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
    }




    static class ViewHolder {
        public ImageView anImageView;
        public TextView aTextView;
    }
}
