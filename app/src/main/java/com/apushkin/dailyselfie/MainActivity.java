package com.apushkin.dailyselfie;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
//TODO - modify the layout of the selfie line in the list
//TODO - delete the Settings menu
//TODO - add the ability to delete selfies
//TODO - modify the ViewSelfie Activity in order to use gestures for changing Views (slideshow)
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        SwipeRefreshLayout.OnRefreshListener {
    private AlarmManager mAlarmManager;
    private Intent mNotificationReceiverIntent;
    private PendingIntent mNotificationReceiverPendingIntent;
    private static final long INITIAL_ALARM_DELAY = 2 * 60 * 1000L;     //Two minutes in millis.
    private static final long TEST_ALARM_DELAY = 30 * 1000L;            //30 seconds.
    private DialogFragment dialogFragment;
    private DialogFragment dialogDeleteFragment;

    private static final String TAG = "DailySelfie";
    static final int REQUEST_IMAGE_CAPTURE = 1;

    public static String getFileName() {
        return FILE_NAME;
    }

    private static final String FILE_NAME = "SelfieData.txt";
    public static final String PATH = "clickedItemPath";

    private SwipeRefreshLayout mSwipeRefreshLayout;

    SelfieItemsListAdapter mAdapter;
    String mCurrentPhotoPath;

    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               dispatchTakePictureIntent();
            }
        });

        mListView = (ListView) findViewById(R.id.selfieListView);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        // Create an Intent to broadcast to the AlarmNotificationReceiver
        mNotificationReceiverIntent = new Intent(MainActivity.this,
                AlarmNotificationReceiver.class);

        // Create a PendingIntent that holds the NotificationReceiverIntent
        mNotificationReceiverPendingIntent = PendingIntent.getBroadcast(
                MainActivity.this, 0, mNotificationReceiverIntent, 0);

        // Set repeating alarm
        mAlarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + INITIAL_ALARM_DELAY,
                INITIAL_ALARM_DELAY,
                mNotificationReceiverPendingIntent);

        mAdapter = new SelfieItemsListAdapter(this.getApplicationContext());

        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SelfieItem selectedItem = (SelfieItem) mAdapter.getItem(position);

                Intent viewSelfieIntent =
                        new Intent(MainActivity.this, ViewSelfieActivity.class);
                viewSelfieIntent.putExtra(PATH, selectedItem.getFilename());
                startActivity(viewSelfieIntent);
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshSelfiesList);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_take_selfie) {
            dispatchTakePictureIntent();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_about) {
            dialogFragment = AlertDialogFragment.newInstance();
            dialogFragment.show(getSupportFragmentManager(), "Alert");
            return true;
        } else if (id == R.id.nav_clear_all) {
            dialogDeleteFragment = DeleteSelfiesDialogFragment.newInstance();
            dialogDeleteFragment.show(getSupportFragmentManager(), "Delete");
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){

            //Create new SelfieItem
            SelfieItem selfieItem = new SelfieItem(new Date(), mCurrentPhotoPath);
            mAdapter.add(selfieItem);

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveItems();
    }

    @Override
    protected void onResume() {
        if (mAdapter.getCount() == 0){
            loadItems();
        }
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 1500);
        super.onResume();
    }

    @Override
    public void onRefresh() {
        //Just for fun!
        mSwipeRefreshLayout.setRefreshing(true);
        // waiting for 3 seconds and hiding the process
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 1500);
    }

    public void stopRefresh(){
        if(mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    }

//---------------------------------- My own helper methods ----------------------------------------
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            }
            catch (IOException ex){
                Log.e(TAG, ex.getMessage());
            }
            if(photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = /*"file:" + */ image.getAbsolutePath();
        return image;
    }

    // Load stored SelfieItems
    private void loadItems() {
        new LoadSelfiesAsyncTask().execute();
    }

    // Save SelfieItems to file
    private void saveItems() {
        PrintWriter writer = null;
        try {
            FileOutputStream fos = openFileOutput(FILE_NAME, MODE_PRIVATE);
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
                    fos)));

            for (int idx = 0; idx < mAdapter.getCount(); idx++) {

                writer.println(mAdapter.getItem(idx));

            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != writer) {
                writer.close();
            }
        }
    }

    private void clearAllSelfies(){
        BufferedReader reader = null;
        try {
            FileInputStream fis = openFileInput(FILE_NAME);
            reader = new BufferedReader(new InputStreamReader(fis));

            String filename = null;

            while (null != (filename = reader.readLine())) {
                File selfieFile = new File(filename);
                boolean deleted = selfieFile.delete();
            }
            mAdapter.clear();
            mAdapter.notifyDataSetChanged();
    } catch (FileNotFoundException e) {
        e.printStackTrace();
        }
      catch (IOException e) {
          e.printStackTrace();
      }
    }

    private void continueDialog(boolean shouldContinue) {
        //If a reconfiguration occurred we need to refresh a link to the dialogFragment
        if (null == dialogFragment) {
            dialogFragment = (DialogFragment) getSupportFragmentManager()
                    .findFragmentByTag("Alert");
        }

        dialogFragment.dismiss();
    }

    private void continueDeleteSelfiesDialog(boolean shouldContinue) {
        //If a reconfiguration occurred we need to refresh a link to the dialogFragment
        if (null == dialogDeleteFragment) {
            dialogDeleteFragment = (DialogFragment) getSupportFragmentManager()
                    .findFragmentByTag("Delete");
        }
        if (shouldContinue){
            clearAllSelfies();
            dialogDeleteFragment.dismiss();


        } else {
            dialogDeleteFragment.dismiss();
        }
    }

    public static class AlertDialogFragment extends DialogFragment {

        public static AlertDialogFragment newInstance() {
            return new AlertDialogFragment();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Get the layout inflater
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View dialogLayout = inflater.inflate(R.layout.dialog, null);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setView(dialogLayout);
            Button buttonOk = (Button) dialogLayout.findViewById(R.id.okButton);

            buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity)getActivity()).continueDialog(true);
                }
            });

            return builder.create();
        }
    }

    public static class DeleteSelfiesDialogFragment extends DialogFragment {

        public static DeleteSelfiesDialogFragment newInstance() {
            return new DeleteSelfiesDialogFragment();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Get the layout inflater
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View dialogLayout = inflater.inflate(R.layout.dialog_for_delete_selfies, null);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setView(dialogLayout);
            Button buttonOk = (Button) dialogLayout.findViewById(R.id.okDeleteButton);
            Button buttonCancel = (Button) dialogLayout.findViewById(R.id.cancelDeleteButton);

            buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity)getActivity()).continueDeleteSelfiesDialog(true);
                }
            });

            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity)getActivity()).continueDeleteSelfiesDialog(false);
                }
            });

            return builder.create();
        }
    }

    private class LoadSelfiesAsyncTask extends AsyncTask<Integer, Void, List<SelfieItem>> {

        @Override
        protected List<SelfieItem> doInBackground(Integer... params) {
            BufferedReader reader = null;
            List<SelfieItem> selfiesList = new ArrayList<>();
            try {
                FileInputStream fis = openFileInput(FILE_NAME);
                reader = new BufferedReader(new InputStreamReader(fis));

                String filename = null;
                Date date = null;

                while (null != (filename = reader.readLine())) {
                    date = SelfieItem.FORMAT.parse(reader.readLine());
                    selfiesList.add(new SelfieItem(date,filename));
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            } finally {
                if (null != reader) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return selfiesList;
        }

        //Adding loaded SelfieItems to the ListAdapter
        @Override
        protected void onPostExecute(List<SelfieItem> selfieItems) {
            super.onPostExecute(selfieItems);
            for (SelfieItem item: selfieItems) {
                mAdapter.add(item);
            }
            mAdapter.notifyDataSetChanged();
        }
    }


}
