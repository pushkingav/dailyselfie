package com.apushkin.dailyselfie;

import android.content.Intent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SelfieItem {
    public final static String DATE = "date";
    public final static String FILENAME = "filename";
    public static final String ITEM_SEP = System.getProperty("line.separator");

    public final static SimpleDateFormat FORMAT = new SimpleDateFormat(
            "yyyyMMdd_HHmmss", Locale.US);

    private Date mDate = new Date();
    private String mFilename = "";


    public SelfieItem(Date date, String filename) {

        this.mDate = date;
        this.mFilename = filename;

    }

    SelfieItem(Intent intent) {
        mFilename = intent.getStringExtra(SelfieItem.FILENAME);

        try {
            mDate = SelfieItem.FORMAT.parse(intent.getStringExtra(SelfieItem.DATE));
        } catch (ParseException e) {
            mDate = new Date();
        }
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        this.mDate = date;
    }

    public String getFilename() {
        return mFilename;
    }

    public void setFilename(String filename) {
        this.mFilename = filename;
    }

    public String toString() {
        return mFilename + ITEM_SEP + FORMAT.format(mDate);
    }


}
